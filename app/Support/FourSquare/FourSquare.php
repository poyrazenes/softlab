<?php

namespace App\Support\FourSquare;


class FourSquare
{
    protected $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getCategories()
    {
        $link = $this->config['base_link'] . '/categories';
        $link .= $this->buildUrl();

        $response = $this->makeRequest($link);
        $categories = $this->parseResponse($response, 'category');
        return $categories;
    }

    public function explore($params)
    {
        $link = $this->config['base_link'] . '/explore';
        $link .= $this->buildUrl($params);

        $response = $this->makeRequest($link);
        $places = $this->parseResponse($response, 'explore');
        return $places;
    }

    public function parseResponse($response, $type = 'category')
    {
        $clean_response = [];
        if (isset($response) && isset($response->response)) {
            if ($type == 'category') {
                if (isset($response->response->categories)) {
                    $clean_response = $this->parseCategories($response->response->categories);
                }
            } else if ($type == 'explore') {
                if (isset($response->response->groups)) {
                    $clean_response = $this->parsePlaces($response->response->groups);
                }
            }
        }

        return $clean_response;
    }

    private function parseCategories($categories, $depth = 0)
    {
        $cats = [];

        if ($depth < 2) {
            try {
                foreach ($categories as $category) {
                    $cats[] = [
                        'id' => $category->id,
                        'name' => $category->name,
                        'plural_name' => $category->pluralName,
                        'short_name' => $category->shortName,
                        'icon' => $category->icon->prefix . '32' . $category->icon->suffix,
                        'sub_categories' => $this->parseCategories($category->categories, $depth + 1)
                    ];
                }
            } catch (\Throwable $exception) {
            }
        }

        return $cats;
    }

    private function parsePlaces($groups)
    {
        $places = [];

        try {
            foreach ($groups as $group) {
                foreach ($group->items as $item) {
                    $place = $item->venue;
                    $places[] = [
                        'id' => $place->id,
                        'name' => $place->name,
                        'latitude' => $place->location->lat ?? '',
                        'longitude' => $place->location->lng ?? '',
                        'city' => $place->location->city ?? '',
                        'state' => $place->location->state ?? '',
                        'country' => $place->location->country ?? '',
                        'full_address' => implode(',', ($place->location->formattedAddress ?? [])),
                        'checkin_count' => $place->stats->checkinsCount ?? 0,
                        'visit_count' => $place->stats->visitsCount ?? 0,
                        'been_here_count' => $place->beenHere->count ?? 0,
                        'here_now' => $place->hereNow->count ?? 0,
                    ];
                }
            }
        } catch (\Throwable $exception) {
        }

        return $places;
    }

    private function buildUrl($params = [])
    {
        $query_string = '?';
        foreach ($this->config['params'] as $key => $value) {
            $query_string .= $key . '=' . $value . '&';
        }

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $query_string .= $key . '=' . $value . '&';
            }
        }

        return trim($query_string, '&');
    }

    private function makeRequest($link)
    {
        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($ch) ;
        curl_close($ch);

        return json_decode($curl_response);
    }
}
