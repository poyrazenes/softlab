<?php

namespace App\Http\Controllers;

use App\Support\FourSquare\FourSquare;

use Cache, Str;

class HomeController extends Controller
{
    public function index()
    {
        return redirect()->action('HomeController@categories');
    }

    public function categories($category = '')
    {
        $categories = Cache::remember('categories', 10, function () {
            $foursquare = new FourSquare(config('foursquare'));
            return $foursquare->getCategories();
        });

        $places = [];
        if (!empty($category)) {
            $params = [
                'query' => $category,
                'near' => 'valetta'
            ];

            $slug = $this->getSlug($category);
            $places = Cache::remember('places_' . $slug, 10, function () use ($params) {
                $foursquare = new FourSquare(config('foursquare'));
                return $foursquare->explore($params);
            });
        }

        return view('categories', [
            'categories' => $categories,
            'category' => $category,
            'places' => $places
        ]);
    }

    private function getSlug($str)
    {
        $str = str_replace(
            ['Ö', 'ö', 'Ü', 'ü', 'Ş', 'ş', 'I', 'ı', 'İ'],
            ['O', 'o', 'U', 'u', 'S', 's', 'i', 'i', 'i'],
            $str
        );
        return Str::slug($str);
    }
}
