<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Softlab Test</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <link rel="stylesheet" href="/web/css/custom.css">
</head>
<body>

<div class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>Categories</h3>
        </div>
        <ul class="list-unstyled components">
            @foreach($categories as $cat)
                <li>
                    <a href="#{{ $cat['id'] }}" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">{{ $cat['name'] }}</a>
                    <ul class="collapse list-unstyled" id="{{ $cat['id'] }}">
                        @if(!empty($cat['sub_categories']))
                            @foreach($cat['sub_categories'] as $sc)
                                <li class="dropdown-items {{ $sc['name'] == $category ? 'active' : '' }}">
                                    <a href="{{ action('HomeController@categories', ['category' => $sc['name']]) }}">{{ $sc['name'] }}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </li>
            @endforeach
        </ul>
    </nav>
    <div class="container-fluid">
        <div class="text-center">
            <h2 style="padding: 20px;">{{ $category }}</h2>
        </div>
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Address</th>
                <th scope="col">Checkin Count</th>
                <th scope="col">Visit Count</th>
                <th scope="col">Here Now</th>
            </tr>
            </thead>
            <tbody>
                @if(!empty($places))
                    @foreach($places as $place)
                        <tr>
                            <td>{{ $place['id'] }}</td>
                            <td>{{ $place['name'] }}</td>
                            <td>{{ $place['full_address'] }}</td>
                            <td>{{ $place['checkin_count'] }}</td>
                            <td>{{ $place['visit_count'] }}</td>
                            <td>{{ $place['here_now'] }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr class="text-center">
                        <td colspan="6" style="font-size: 20px; font-weight: 400;">Place Not Found</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        if ($('.dropdown-items.active').length > 0) {
            $('.dropdown-items.active').parent('ul.collapse').addClass('show');
        }

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>
</body>
</html>
